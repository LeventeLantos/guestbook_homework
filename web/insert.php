<?php
include_once('dbconn.php');

//Insert data to postgres
if (isset($_POST['submit'])) {
  $title = $_POST['title'];
  $body = $_POST['body'];
  $stars = $_POST['stars'];
  // Conditions to do!!!!!
  $sql = 'INSERT INTO test01(title, body, stars) VALUES(:title, :body, :stars)';
  $statement = $conn->prepare($sql);
  $statement->execute([':title' => $title, ':body' => $body, ':stars' => $stars]);
  header('Location: /index.php', TRUE, 303);
}
